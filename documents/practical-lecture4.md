## Basic querying

```go
package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"time"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	sc "github.com/hyperledger/fabric/protos/peer"
)

type SmartContract struct {
}

type Coordinates struct {
	Latitude  float64 `json:"latitude"`
	Longitude float64 `json:"longitude"`
}

type ParkingPlace struct {
	Id          string      `json:"id"`
	Location    Coordinates `json:"location"`
	IsAvailable bool        `json:"isAvailable"`
}

type ParkingSession struct {
	ParkingSpaceID string  `json:"parkingSpaceID"`
	Customer       string  `json:"customer"`
	StartDateTime  string  `json:"startDateTime"`
	EndDateTime    string  `json:"endDateTime"`
	Cost           float64 `json:"cost"`
}

func (s *SmartContract) Init(APIstub shim.ChaincodeStubInterface) sc.Response {
	return shim.Success(nil)
}

func (s *SmartContract) Invoke(APIstub shim.ChaincodeStubInterface) sc.Response {
	function, args := APIstub.GetFunctionAndParameters()
	if function == "startParkingSession" {
		return s.startParkingSession(APIstub, args)
	} else if function == "stopParkingSession" {
		return s.stopParkingSession(APIstub, args)
	} else if function == "initLedger" {
    return s.initLedger(APIstub)
  } else if function == "queryAllParkingPlaces" {
		return s.queryAllParkingPlaces(APIstub)
  } else if function == "queryAvailableParkingPlaces" {
		return s.queryAvailableParkingPlaces(APIstub, args)
  }

	return shim.Error("Invalid Smart Contract function name.")
}

func (s *SmartContract) startParkingSession(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {
	if len(args) != 2 {
		return shim.Error("Incorrect number of arguments. Expecting 2")
	}

	txTimestamp, _ := APIstub.GetTxTimestamp()
	timestamp := time.Unix(txTimestamp.GetSeconds(), int64(txTimestamp.GetNanos()))
	session := ParkingSession{
		ParkingSpaceID: args[0],
		Customer:       args[1],
		StartDateTime:  timestamp.String(),
		EndDateTime:    "",
		Cost:           0}

	sessionAsBytes, _ := json.Marshal(session)
	APIstub.PutState("ID123", sessionAsBytes)

	fmt.Printf("WROTE: %s\n", sessionAsBytes)
	return shim.Success(nil)
}

func (s *SmartContract) stopParkingSession(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {
	txTimestamp, _ := APIstub.GetTxTimestamp()
	timestamp := time.Unix(txTimestamp.GetSeconds(), int64(txTimestamp.GetNanos()))

	sessionAsBytes, _ := APIstub.GetState("ID123")
	session := ParkingSession{}

	json.Unmarshal(sessionAsBytes, &session)
	session.EndDateTime = timestamp.String()

	startDateTime, _ := time.Parse(time.RFC3339, session.StartDateTime)
	endDateTime, _ := time.Parse(time.RFC3339, session.EndDateTime)

	fmt.Printf("start: %s, end: %s\n", startDateTime.String(), endDateTime.String())

	sessionDuration := endDateTime.Sub(startDateTime)

	fmt.Printf("session duration: %s\n", sessionDuration.String())

	session.Cost = sessionDuration.Minutes() * 0.05

	sessionAsBytes, _ = json.Marshal(session)
	APIstub.PutState("ID123", sessionAsBytes)

	fmt.Printf("WROTE: %s\n", sessionAsBytes)
	return shim.Success(nil)
}

func (s *SmartContract) initLedger(APIstub shim.ChaincodeStubInterface) sc.Response {
	places := []ParkingPlace{
		ParkingPlace{
			Id:          "PK001",
			Location:    Coordinates{Latitude: 58.377769, Longitude: 26.7278297},
			IsAvailable: true},
		ParkingPlace{
			Id:          "PK002",
			Location:    Coordinates{Latitude: 58.3747077, Longitude: 26.736103},
			IsAvailable: false}}

	i := 0
	for i < len(places) {
		placeAsBytes, _ := json.Marshal(places[i])
		APIstub.PutState(places[i].Id, placeAsBytes)
		i = i + 1
	}

	return shim.Success(nil)
}

func (s *SmartContract) queryAllParkingPlaces(APIstub shim.ChaincodeStubInterface) sc.Response {
	startKey := "PK000"
	endKey := "PK999"

	resultsIterator, err := APIstub.GetStateByRange(startKey, endKey)
	if err != nil {
		return shim.Error(err.Error())
	}

	defer resultsIterator.Close()

	var buffer bytes.Buffer
	buffer.WriteString("[")
	bArrayMemberAlreadyWritten := false

	for resultsIterator.HasNext() {
		queryResponse, err := resultsIterator.Next()
		if err != nil {
			return shim.Error(err.Error())
		}

		if bArrayMemberAlreadyWritten == true {
			buffer.WriteString(",")
		}

		buffer.WriteString("{\"Key\":")
		buffer.WriteString("\"")
		buffer.WriteString(queryResponse.Key)
		buffer.WriteString("\"")
		buffer.WriteString(", \"Record\":")
		buffer.WriteString(string(queryResponse.Value))
		buffer.WriteString("}")
		bArrayMemberAlreadyWritten = true
	}
	buffer.WriteString("]")
	return shim.Success(buffer.Bytes())
}

func (s *SmartContract) queryAvailableParkingPlaces(APIstub shim.ChaincodeStubInterface) sc.Response {
	query := `{
		"selector": {
			"IsAvailable": {
				"$eq": true
			}
		}
	}`

	resultsIterator, err := APIstub.GetQueryResult(query)
	if err != nil {
		return shim.Error(err.Error())
	}

	defer resultsIterator.Close()

	var buffer bytes.Buffer
	buffer.WriteString("[")
	bArrayMemberAlreadyWritten := false

	for resultsIterator.HasNext() {
		queryResponse, err := resultsIterator.Next()
		if err != nil {
			return shim.Error(err.Error())
		}

		if bArrayMemberAlreadyWritten == true {
			buffer.WriteString(",")
		}

		buffer.WriteString("{\"Key\":")
		buffer.WriteString("\"")
		buffer.WriteString(queryResponse.Key)
		buffer.WriteString("\"")
		buffer.WriteString(", \"Record\":")
		buffer.WriteString(string(queryResponse.Value))
		buffer.WriteString("}")
		bArrayMemberAlreadyWritten = true
	}
	buffer.WriteString("]")
	return shim.Success(buffer.Bytes())
}

func main() {
	shim.Start(new(SmartContract))
}
```

## Version with geohash

```go
package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"strconv"
	"time"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	sc "github.com/hyperledger/fabric/protos/peer"
	"github.com/pierrre/geohash"
)

type SmartContract struct {
}

type Coordinates struct {
	Latitude  float64 `json:"latitude"`
	Longitude float64 `json:"longitude"`
	Geohash   string  `json:"geohash"`
}

type ParkingPlace struct {
	Id          string      `json:"id"`
	Location    Coordinates `json:"location"`
	IsAvailable bool        `json:"isAvailable"`
}

type ParkingSession struct {
	ParkingSpaceID string  `json:"parkingSpaceID"`
	Customer       string  `json:"customer"`
	StartDateTime  string  `json:"startDateTime"`
	EndDateTime    string  `json:"endDateTime"`
	Cost           float64 `json:"cost"`
}

func (s *SmartContract) Init(APIstub shim.ChaincodeStubInterface) sc.Response {
	return shim.Success(nil)
}

func (s *SmartContract) Invoke(APIstub shim.ChaincodeStubInterface) sc.Response {
	function, args := APIstub.GetFunctionAndParameters()
	if function == "startParkingSession" {
		return s.startParkingSession(APIstub, args)
	} else if function == "stopParkingSession" {
		return s.stopParkingSession(APIstub, args)
	} else if function == "initLedger" {
    return s.initLedger(APIstub)
  } else if function == "queryAllParkingPlaces" {
		return s.queryAllParkingPlaces(APIstub)
  } else if function == "queryAvailableParkingPlaces" {
		return s.queryAvailableParkingPlaces(APIstub, args)
	}

	return shim.Error("Invalid Smart Contract function name.")
}

func (s *SmartContract) startParkingSession(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {
	if len(args) != 2 {
		return shim.Error("Incorrect number of arguments. Expecting 2")
	}

	txTimestamp, _ := APIstub.GetTxTimestamp()
	timestamp := time.Unix(txTimestamp.GetSeconds(), int64(txTimestamp.GetNanos()))
	session := ParkingSession{
		ParkingSpaceID: args[0],
		Customer:       args[1],
		StartDateTime:  timestamp.String(),
		EndDateTime:    "",
		Cost:           0}

	sessionAsBytes, _ := json.Marshal(session)
	APIstub.PutState("ID123", sessionAsBytes)

	fmt.Printf("WROTE: %s\n", sessionAsBytes)
	return shim.Success(nil)
}

func (s *SmartContract) stopParkingSession(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {
	txTimestamp, _ := APIstub.GetTxTimestamp()
	timestamp := time.Unix(txTimestamp.GetSeconds(), int64(txTimestamp.GetNanos()))

	sessionAsBytes, _ := APIstub.GetState("ID123")
	session := ParkingSession{}

	json.Unmarshal(sessionAsBytes, &session)
	session.EndDateTime = timestamp.String()

	startDateTime, _ := time.Parse(time.RFC3339, session.StartDateTime)
	endDateTime, _ := time.Parse(time.RFC3339, session.EndDateTime)

	fmt.Printf("start: %s, end: %s\n", startDateTime.String(), endDateTime.String())

	sessionDuration := endDateTime.Sub(startDateTime)

	fmt.Printf("session duration: %s\n", sessionDuration.String())

	session.Cost = sessionDuration.Minutes() * 0.05

	sessionAsBytes, _ = json.Marshal(session)
	APIstub.PutState("ID123", sessionAsBytes)

	fmt.Printf("WROTE: %s\n", sessionAsBytes)
	return shim.Success(nil)
}

func (s *SmartContract) initLedger(APIstub shim.ChaincodeStubInterface) sc.Response {
	places := []ParkingPlace{
		ParkingPlace{
			Id: "PK001",
			Location: Coordinates{
				Latitude:  58.377769,
				Longitude: 26.7278297,
				Geohash:   geohash.Encode(58.377769, 26.7278297, 12)},
			IsAvailable: true},
		ParkingPlace{
			Id: "PK002",
			Location: Coordinates{
				Latitude:  58.3747077,
				Longitude: 26.736103,
				Geohash:   geohash.Encode(58.3747077, 26.736103, 12)},
			IsAvailable: false}}

	i := 0
	for i < len(places) {
		placeAsBytes, _ := json.Marshal(places[i])
		APIstub.PutState(places[i].Id, placeAsBytes)
		i = i + 1
	}

	return shim.Success(nil)
}

func (s *SmartContract) queryAllParkingPlaces(APIstub shim.ChaincodeStubInterface) sc.Response {
	startKey := "PK000"
	endKey := "PK999"

	resultsIterator, err := APIstub.GetStateByRange(startKey, endKey)
	if err != nil {
		return shim.Error(err.Error())
	}

	defer resultsIterator.Close()

	var buffer bytes.Buffer
	buffer.WriteString("[")
	bArrayMemberAlreadyWritten := false

	for resultsIterator.HasNext() {
		queryResponse, err := resultsIterator.Next()
		if err != nil {
			return shim.Error(err.Error())
		}

		if bArrayMemberAlreadyWritten == true {
			buffer.WriteString(",")
		}

		buffer.WriteString("{\"Key\":")
		buffer.WriteString("\"")
		buffer.WriteString(queryResponse.Key)
		buffer.WriteString("\"")
		buffer.WriteString(", \"Record\":")
		buffer.WriteString(string(queryResponse.Value))
		buffer.WriteString("}")
		bArrayMemberAlreadyWritten = true
	}
	buffer.WriteString("]")
	return shim.Success(buffer.Bytes())
}

func (s *SmartContract) queryAvailableParkingPlaces(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {
	latitude, _ := strconv.ParseFloat(args[1], 64)  // Add code for error handling
	longitude, _ := strconv.ParseFloat(args[3], 64) // Add code for error handling

	location := geohash.Encode(latitude, longitude, 7) // First 7 characters, precision <= 173 m
	query := fmt.Sprintf(`{
		"selector": {
			"$and": [
				"IsAvailable": {
					"$eq": true
				},
				"Location.Geohash": {
					"$regex": "%s"
				}
			]
		}
	}`, location)

	resultsIterator, err := APIstub.GetQueryResult(query)
	if err != nil {
		return shim.Error(err.Error())
	}

	defer resultsIterator.Close()

	var buffer bytes.Buffer
	buffer.WriteString("[")
	bArrayMemberAlreadyWritten := false

	for resultsIterator.HasNext() {
		queryResponse, err := resultsIterator.Next()
		if err != nil {
			return shim.Error(err.Error())
		}

		if bArrayMemberAlreadyWritten == true {
			buffer.WriteString(",")
		}

		buffer.WriteString("{\"Key\":")
		buffer.WriteString("\"")
		buffer.WriteString(queryResponse.Key)
		buffer.WriteString("\"")
		buffer.WriteString(", \"Record\":")
		buffer.WriteString(string(queryResponse.Value))
		buffer.WriteString("}")
		bArrayMemberAlreadyWritten = true
	}
	buffer.WriteString("]")
	return shim.Success(buffer.Bytes())
}

func main() {
	shim.Start(new(SmartContract))
}
```

As for the practical associated with lecture3, you will need to open three terminals. The interaction through terminal 1 remains the same. Below, you will find the sequence of commands that you can use to test the new application.

### Terminal 2
```sh
cd parking/
go get -u github.com/pierrre/geohash
go build
CORE_PEER_ADDRESS=peer:7052 CORE_CHAINCODE_ID_NAME=mycc:0 ./parking
```

### Terminal 3

```sh
go get -u github.com/pierrre/geohash
peer chaincode install -p chaincodedev/chaincode/parking -n mycc -v 0
peer chaincode instantiate -n mycc -v 0 -c '{"Args":[]}' -C myc
peer chaincode invoke -n mycc -c '{"Args":["initLedger"]}' -C myc
peer chaincode invoke -n mycc -c '{"Args":["queryAllParkingPlaces"]}' -C myc
peer chaincode invoke -n mycc -c '{"Args":["queryAvailableParkingPlaces", "latitude", "58.377769", "longitude", "26.7278297"]}' -C myc
```