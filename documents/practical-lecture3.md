## Set up

Assuming that you have already downloaded fabric binaries and sample applications, in this session, we are going to create our first version of the parking application.

Start by locating the "fabric-samples/" folder. Inside such folder, move to "chaincode" and create there a new folder called "parking". Inside that folder, open an editor and create there a file called "parking.go". Copy the following snippet into "parking.go".

```go
package main

import (
	"encoding/json"
	"fmt"
	"time"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	sc "github.com/hyperledger/fabric/protos/peer"
)

type SmartContract struct {
}

type ParkingSession struct {
	ParkingSpaceID string  `json:"parkingSpaceID"`
	Customer       string  `json:"customer"`
	StartDateTime  string  `json:"startDateTime"`
	EndDateTime    string  `json:"endDateTime"`
	Cost           float64 `json:"cost"`
}

func (s *SmartContract) Init(APIstub shim.ChaincodeStubInterface) sc.Response {
	return shim.Success(nil)
}

func (s *SmartContract) Invoke(APIstub shim.ChaincodeStubInterface) sc.Response {
	function, args := APIstub.GetFunctionAndParameters()
	if function == "startParkingSession" {
		return s.startParkingSession(APIstub, args)
	} else if function == "stopParkingSession" {
		return s.stopParkingSession(APIstub, args)
	}
	return shim.Error("Invalid Smart Contract function name.")
}

func (s *SmartContract) startParkingSession(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {
	if len(args) != 2 {
		return shim.Error("Incorrect number of arguments. Expecting 2")
	}

	txTimestamp, _ := APIstub.GetTxTimestamp()
	timestamp := time.Unix(txTimestamp.GetSeconds(), int64(txTimestamp.GetNanos()))
	session := ParkingSession{
		ParkingSpaceID: args[0],
		Customer:       args[1],
		StartDateTime:  timestamp.String(),
		EndDateTime:    "",
		Cost:           0}

	sessionAsBytes, _ := json.Marshal(session)
	APIstub.PutState("ID123", sessionAsBytes)

	fmt.Printf("WROTE: %s\n", sessionAsBytes)
	return shim.Success(nil)
}

func (s *SmartContract) stopParkingSession(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {
	txTimestamp, _ := APIstub.GetTxTimestamp()
	timestamp := time.Unix(txTimestamp.GetSeconds(), int64(txTimestamp.GetNanos()))

	sessionAsBytes, _ := APIstub.GetState("ID123")
	session := ParkingSession{}

	json.Unmarshal(sessionAsBytes, &session)
	session.EndDateTime = timestamp.String()

	startDateTime, _ := time.Parse(time.RFC3339, session.StartDateTime)
	endDateTime, _ := time.Parse(time.RFC3339, session.EndDateTime)

	sessionDuration := endDateTime.Sub(startDateTime)

	session.Cost = sessionDuration.Minutes() * 0.05

	sessionAsBytes, _ = json.Marshal(session)
	APIstub.PutState("ID123", sessionAsBytes)

	fmt.Printf("WROTE: %s\n", sessionAsBytes)
	return shim.Success(nil)
}

func main() {
	shim.Start(new(SmartContract))
}
```



## Terminal 1

```sh
cd chaincode-docker-devmode
docker-compose -f docker-compose-simple.yaml up
```

The above will start a *development* instance of Hyperledger fabric. Note that the terminal will be blocked and, it is for this reason that we need to open other two terminals.

## Terminal 2

Over the second terminal, run the following command:

```sh
docker exec -it chaincode bash
```

The above will open a terminal session on the node that will run the smart contract (aka "chaincode"). Note that docker compose, mounted the folder where our code is located inside the ndoe "chaincode", such that the code will be available for compilation and later for deployment into fabric.

Once inside the "chaincode" node, run the following commands to compile the smart contract and to run locally the code.

```sh
cd parking/
go build
CORE_PEER_ADDRESS=peer:7052 CORE_CHAINCODE_ID_NAME=mycc:0 ./parking
```

## Terminal 3

In the third terminal, we are going to connect the chaincode that we started in terminal 2 to the business network (cf. "mycc"), and then, we are going to instantiate the contract, connecting it to a channel (cf. "myc"). The two last commands will start and stop a parking session for demonstration purposes.

Start by connecting to the node that has fabric "cli" component running.

```sh
docker exec -it cli bash
```

There, you will install, instantiate and invoke the chain code.

```sh
peer chaincode install -p chaincodedev/chaincode/parking -n mycc -v 0
peer chaincode instantiate -n mycc -v 0 -c '{"Args":[]}' -C myc
peer chaincode invoke -n mycc -c '{"Args":["startParkingSession", "ID123", "frodo"]}' -C myc
peer chaincode invoke -n mycc -c '{"Args":["stopParkingSession"]}' -C myc
```

If you switch to terminal 2, you will see that the smart contract prints out some feedback into the console.

## Finish the development network and clean up

To stop the development instance of fabric, exit Terminal 3. Stop the application that is running in Terminal 2, using a Ctrl-C, and then exit Terminal 2. Now, stop fabric by using Ctrl-C over Terminal 1. Do not forget to clean up the docker containers to avoid clashes with future sessions. To that end, use the command `docker rm $(docker ps -aq)`.