'use strict';

var Fabric_Client = require('fabric-client');
var path = require('path');

var store_path = path.join(__dirname, 'hfc-key-store');
console.log('Store path:'+store_path);

module.exports.signIn = function(fabric_client, username) {
  return Fabric_Client.newDefaultKeyValueStore({ path: store_path})
          .then((state_store) => {

            fabric_client.setStateStore(state_store);
            var crypto_suite = Fabric_Client.newCryptoSuite();
            
            var crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
            crypto_suite.setCryptoKeyStore(crypto_store);
            fabric_client.setCryptoSuite(crypto_suite);

            return fabric_client.getUserContext(username, true);
          });
}