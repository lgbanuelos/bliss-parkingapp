'use strict';

var Fabric_Client = require('fabric-client');
var signIn = require('./authenticateUser').signIn;

var fabric_client = new Fabric_Client();

var channel = fabric_client.newChannel('myc');
var peer = fabric_client.newPeer('grpc://localhost:7051');
channel.addPeer(peer);
var order = fabric_client.newOrderer('grpc://localhost:7050')
channel.addOrderer(order);

var tx_id = null;

signIn(fabric_client, 'user1').then((user_from_store) => {
	if (!(user_from_store && user_from_store.isEnrolled())) {
		throw new Error('Failed to get user1.... run registerUser.js');
	}
console.log("before");
	tx_id = fabric_client.newTransactionID();
	console.log("after");
	console.log("Assigning transaction_id: ", tx_id._transaction_id);

	var request = {
		chaincodeId: 'mycc', chainId: 'myc', txId: tx_id,
		fcn: 'queryAllParkingPlaces', args: [] //'58.377769', '26.7278297']
	};

	return channel.sendTransactionProposal(request);
}).then((query_responses) => {
	console.log("Query has completed, checking results");
	if (query_responses && query_responses.length == 1) {
		if (query_responses[0] instanceof Error) {
			console.error("error from query = ", query_responses[0]);
		} else {
			console.log("Response is ", query_responses[0].toString());
		}
	} else {
		console.log("No payloads were returned from query");
	}
}).catch((err) => {
	console.error('Failed to query successfully :: ' + err);
});
